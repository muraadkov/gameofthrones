﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThrones14._05._2019
{
        public class Character
        {
            public IList<string> titles { get; set; }
            public IList<string> spouse { get; set; }
            public IList<string> children { get; set; }
            public IList<string> allegiance { get; set; }
            public IList<string> books { get; set; }
            public int plod { get; set; }
            public IList<object> longevity { get; set; }
            public double plodB { get; set; }
            public int plodC { get; set; }
            public IList<double> longevityB { get; set; }
            public IList<object> longevityC { get; set; }
            public string _id { get; set; }
            public string name { get; set; }
            public string slug { get; set; }
            public string gender { get; set; }
            public string culture { get; set; }
            public string house { get; set; }
            public bool alive { get; set; }
            public DateTime createdAt { get; set; }
            public DateTime updatedAt { get; set; }
            public int __v { get; set; }
            public Pagerank pagerank { get; set; }
            public string id { get; set; }
            public string image { get; set; }
            public int? birth { get; set; }
            public string placeOfDeath { get; set; }
            public int? death { get; set; }
            public string placeOfBirth { get; set; }
            public int? longevityStartB { get; set; }
            public string father { get; set; }
            public string mother { get; set; }
            public string heir { get; set; }
        public override string ToString()
        {
            return name;
        }
    }
}
